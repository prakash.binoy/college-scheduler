require "administrate/base_dashboard"

class InstructorDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    name: Field::String,
    available_hours: Field::String.with_options(searchable: false),
    can_teach_subjects: SelectField.with_options(
                          display_proc: -> data { Subject.find(data.reject { |i| i.blank? }).map(&:name).join(', ') },
                          select_values: Subject.all.map {|s| [s.name, s.id]},
                          select_options: {multiple: true}
                        ),
    max_schedulable_hours: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :available_hours,
    :can_teach_subjects,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :name,
    :available_hours,
    :can_teach_subjects,
    :max_schedulable_hours,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :name,
    :available_hours,
    :can_teach_subjects,
    :max_schedulable_hours,
  ].freeze

  # Overwrite this method to customize how instructors are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(instructor)
  #   "Instructor ##{instructor.id}"
  # end


  def permitted_attributes
    [:name, :available_hours, :max_schedulable_hours, :can_teach_subjects => []]
  end
end
