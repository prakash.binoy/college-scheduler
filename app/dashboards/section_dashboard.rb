require "administrate/base_dashboard"

class SectionDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    quarter: Field::BelongsTo,
    id: Field::Number,
    level_id: Field::Number,
    session_id: EnumField,
    slot_id: EnumField,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :quarter,
    :id,
    :level_id,
    :session_id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :quarter,
    :id,
    :level_id,
    :session_id,
    :slot_id,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :quarter,
    :level_id,
    :session_id,
    :slot_id,
  ].freeze

  # Overwrite this method to customize how sections are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(section)
  #   "Section ##{section.id}"
  # end
end
