require "administrate/field/base"

class EnumField < Administrate::Field::Base
  def to_s
    data.titleize unless data.nil?
  end
end
