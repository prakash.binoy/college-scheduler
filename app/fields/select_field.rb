require "administrate/field/base"

class SelectField < Administrate::Field::Base
  def to_s
    options[:display_proc].call(data)
  end

  def select_field_values(form_builder)
    options[:select_values]
  end

  def select_field_options
    options[:select_options]
  end
end

