class Instructor < ApplicationRecord
  def available_hours=(attribute)
    write_attribute(:available_hours, JSON.parse(attribute))
  end
end
