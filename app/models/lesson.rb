class Lesson < ApplicationRecord
  enum lesson_type: { core_class: 0, mentor_session: 1 }
  enum day: {sunday: 0, monday: 1, tuesday: 2, wednesday: 3, thursday: 4, friday: 5, saturday: 6}

  belongs_to :section
  belongs_to :room
  belongs_to :instructor
  belongs_to :subject
  belongs_to :student, optional: true
end
