class Section < ApplicationRecord
  belongs_to :quarter

  enum session_id: { morning: 0, afternoon: 1, evening: 2, night: 3 }
  enum slot_id: { mon_wed_fri: 0, tue_thu_sat: 1 }
end
