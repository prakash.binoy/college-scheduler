Rails.application.routes.draw do
  namespace :admin do
    resources :curriculums
    resources :instructors
    resources :lessons
    resources :quarters do
      get 'calendar', on: :member
    end
    resources :rooms
    resources :sections
    resources :students
    resources :subjects

    root to: "quarters#index"
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
