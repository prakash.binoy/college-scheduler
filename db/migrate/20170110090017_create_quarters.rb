class CreateQuarters < ActiveRecord::Migration[5.0]
  def change
    create_table :quarters do |t|
      t.string :name
      t.integer :start_month
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
