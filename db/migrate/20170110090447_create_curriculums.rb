class CreateCurriculums < ActiveRecord::Migration[5.0]
  def change
    create_table :curriculums do |t|
      t.references :quarter
      t.integer :level_id
      t.jsonb :details

      t.timestamps
    end
  end
end
