class CreateSections < ActiveRecord::Migration[5.0]
  def change
    create_table :sections do |t|
      t.references :quarter
      t.integer :level_id
      t.integer :session_id
      t.integer :slot_id

      t.timestamps
    end
  end
end
