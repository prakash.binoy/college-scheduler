class CreateInstructors < ActiveRecord::Migration[5.0]
  def change
    create_table :instructors do |t|
      t.string :name
      t.jsonb :available_hours, default: {}
      t.jsonb :can_teach_subjects, default: []
      t.integer :max_schedulable_hours

      t.timestamps
    end
  end
end
