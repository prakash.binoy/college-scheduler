class CreateStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :students do |t|
      t.string :uid
      t.string :name
      t.references :section

      t.timestamps
    end
  end
end
