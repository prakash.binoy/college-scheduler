class CreateLessons < ActiveRecord::Migration[5.0]
  def change
    create_table :lessons do |t|
      t.integer :lesson_type
      t.integer :day
      t.time :start_time
      t.time :end_time
      t.references :section
      t.references :room
      t.references :instructor
      t.references :subject
      t.references :student

      t.timestamps
    end
  end
end
